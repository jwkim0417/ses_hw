import React from "react";
import "./App.css";

import Footer from "./components/Footer";
import Main from "./components/Main";
import Navbar from "./components/Navbar";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <div>
          <Navbar />
        </div>
        <div>
          <Main />
        </div>
        <div>
          <Footer />
        </div>
      </div>
    );
  }
}

export default App;
