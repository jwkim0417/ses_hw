import { FirstPage, SecondPage, ThirdPage, FourthPage } from "../modules";
import { combineReducers } from "redux";

const footerInitialState = {
  title: ""
};

const footer = (state = footerInitialState, action: any) => {
  switch (action.type) {
    case FirstPage:
      return Object.assign({}, state, {
        title: "1번"
      });
    case SecondPage:
      return Object.assign({}, state, {
        title: "2번"
      });
    case ThirdPage:
      return Object.assign({}, state, {
        title: "3번"
      });
    case FourthPage:
      return Object.assign({}, state, {
        title: "4번"
      });
    default:
      return state;
  }
};

const footerApp = combineReducers({
  footer
});

export default footerApp as any;
