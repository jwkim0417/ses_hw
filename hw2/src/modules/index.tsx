export const FirstPage = "1번";
export const SecondPage = "2번";
export const ThirdPage = "3번";
export const FourthPage = "4번";

export function firstpage() {
  return {
    type: FirstPage
  };
}

export function secondpage() {
  return {
    type: SecondPage
  };
}

export function thirdpage() {
  return {
    type: ThirdPage
  };
}

export function fourthpage() {
  return {
    type: FourthPage
  };
}
