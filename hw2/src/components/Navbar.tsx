import React from "react";
import { connect } from "react-redux";
import { firstpage, secondpage, thirdpage, fourthpage } from "../modules";

interface MyProps {
  onFirstPage: any;
  onSecondPage: any;
  onThirdPage: any;
  onFourthPage: any;
}

interface MyState {}

class Navbar extends React.Component<MyProps, MyState> {
  render() {
    return (
      <div>
        <a href="#" onClick={this.props.onFirstPage}>
          navbar1
        </a>{" "}
        <a href="#" onClick={this.props.onSecondPage}>
          navbar2
        </a>{" "}
        <a href="#" onClick={this.props.onThirdPage}>
          navbar3
        </a>{" "}
        <a href="#" onClick={this.props.onFourthPage}>
          navbar4
        </a>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch: any) => {
  return {
    onFirstPage: () => dispatch(firstpage()),
    onSecondPage: () => dispatch(secondpage()),
    onThirdPage: () => dispatch(thirdpage()),
    onFourthPage: () => dispatch(fourthpage())
  };
};

export default connect(
  undefined,
  mapDispatchToProps
)(Navbar);
