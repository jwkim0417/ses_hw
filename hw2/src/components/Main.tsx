import React from "react";
import { connect } from "react-redux";

interface MyProps {
  title: string;
}

interface MyState {}

class Main extends React.Component<MyProps, MyState> {
  render() {
    const title = this.props.title;
    return <p>{title}</p>;
  }
}

let mapStateToProps = (state: any) => {
  return {
    title: state.footer.title
  };
};

export default connect(mapStateToProps)(Main);
