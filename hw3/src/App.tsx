import React from "react";
import "./App.css";

import { BrowserRouter as Router, Route } from "react-router-dom";

import Comments from "./components/comments";
import Post from "./components/post";
import Posts from "./components/posts";

const App: React.FC = () => {
  return (
    <Router>
      <div className="App">
        <p>hw3</p>
        <Route exact path="/" render={() => <Posts />} />
        <Route path="/post/:id" render={() => <Post />} />
        <Route path="/comments/:id" render={() => <Comments />} />
      </div>
    </Router>
  );
};

export default App;
