import React, { useState, useEffect } from "react";

import { Link } from "react-router-dom";

export interface post {
  userId: number;
  id: number;
  title: string;
  body: string;
}

const Posts = () => {
  const [loading, setLoading] = useState(true);
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then(response => response.json())
      .then(posts => {
        setPosts(posts);
        setLoading(false);
      });
  }, []);

  if (loading) return <div>Loading...</div>;

  return (
    <div>
      {posts.map((post: post) => (
        <li key={post.id}>
          <Link to={"/post/" + post.id}>{post.title}</Link>
        </li>
      ))}
    </div>
  );
};

export default Posts;
