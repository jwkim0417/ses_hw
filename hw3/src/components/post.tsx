import React, { useState, useEffect } from "react";
import { withRouter, RouteComponentProps } from "react-router";
import { Link } from "react-router-dom";

export interface post {
  userId: number;
  id: number;
  title: string;
  body: string;
}

interface MatchParams {
  id: string;
}

const Post: React.SFC<RouteComponentProps<MatchParams>> = ({ match }) => {
  const [url] = useState(
    "https://jsonplaceholder.typicode.com/posts/" + match.params.id
  );
  const [loading, setLoading] = useState(true);
  const [myPost, setMyPost] = useState<post>({
    userId: 0,
    id: 0,
    title: "",
    body: ""
  });

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(post => {
        setMyPost(post);
        setLoading(false);
      });
  }, []);

  if (loading) return <div>Loading...</div>;

  return (
    <div>
      <p>{myPost.userId}</p>
      <p>{myPost.id}</p>
      <p>{myPost.title}</p>
      <p>{myPost.body}</p>
      <Link to={"/comments/" + myPost.id}>comments</Link>
    </div>
  );
};

export default withRouter(Post);
