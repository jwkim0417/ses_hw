import React, { useState, useEffect } from "react";
import { withRouter, RouteComponentProps } from "react-router";
import { Link } from "react-router-dom";

export interface comment {
  postId: number;
  id: number;
  name: string;
  email: string;
  body: string;
}

interface MatchParams {
  id: string;
}

const Comments: React.SFC<RouteComponentProps<MatchParams>> = ({ match }) => {
  const [url] = useState(
    "https://jsonplaceholder.typicode.com/comments?postId=" + match.params.id
  );
  const [loading, setLoading] = useState(true);
  const [comments, setComments] = useState([]);

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(comments => {
        setComments(comments);
        setLoading(false);
        console.log(url);
      });
  }, []);

  if (loading) return <div>Loading...</div>;

  return (
    <div>
      {comments.map((comment: comment) => (
        <li key={comment.id}>
          <p>Comment {comment.id}</p>
          <p>User: {comment.name}</p>
          <p>Email: {comment.email}</p>
          <p>Comment: {comment.body}</p>
        </li>
      ))}
      <Link to="/">Home</Link>
    </div>
  );
};

export default withRouter(Comments);
