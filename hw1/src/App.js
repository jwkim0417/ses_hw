import React, { useState } from "react";
import "./App.css";

const Navbar = ({ setMyState }) => {
  return (
    <div>
      <a href="#" onClick={() => setMyState("1번")}>
        navbar1
      </a>{" "}
      <a href="#" onClick={() => setMyState("2번")}>
        navbar2
      </a>{" "}
      <a href="#" onClick={() => setMyState("3번")}>
        navbar3
      </a>{" "}
      <a href="#" onClick={() => setMyState("4번")}>
        navbar4
      </a>
    </div>
  );
};

const Main = ({ myState }) => {
  return <p>{myState}</p>;
};

const Footer = () => {
  return <p>footer</p>;
};

const App = () => {
  const [myState, setMyState] = useState("");
  return (
    <div className="App">
      <div>
        <Navbar setMyState={setMyState} />
      </div>
      <div>
        <Main myState={myState} />
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
};

export default App;
